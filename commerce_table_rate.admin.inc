<?php

/**
 * @file
 * Administrative page callbacks for the commerce_table_rate module.
 */

/**
 * Builds the form for adding and editing table rate services.
 */
function commerce_table_rate_service_form($form, &$form_state, $shipping_service) {
  // @see: commerce_colissimo.admin.inc
  // Store the initial shipping service in the form state.
  $form_state['shipping_service'] = $shipping_service;
  $form['table_rate'] = array(
    '#tree' => TRUE,
  );

  $form['table_rate']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $shipping_service['title'],
    '#description' => t('The administrative title of this table rate. It is recommended that this title begin with a capital letter and contain only letters, numbers, and spaces.'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
    '#field_suffix' => ' <small id="edit-table-rate-title-suffix">' . t('Machine name: @name', array('@name' => $shipping_service['name'])) . '</small>',
  );

  if (empty($shipping_service['name'])) {
    $form['table_rate']['name'] = array(
      '#type' => 'machine_name',
      '#title' => t('Machine name'),
      '#default_value' => $shipping_service['name'],
      '#maxlength' => 32,
      '#required' => TRUE,
      '#machine_name' => array(
        'exists' => 'commerce_shipping_service_load',
        'source' => array('table_rate', 'title'),
      ),
      '#description' => t('The machine-name of this table rate. This name must contain only lowercase letters, numbers, and underscores. It must be unique.'),
    );
  }
  else {
    $form['table_rate']['name'] = array(
      '#type' => 'value',
      '#value' => $shipping_service['name'],
    );
  }

  $form['table_rate']['display_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display title'),
    '#default_value' => $shipping_service['display_title'],
    '#description' => t('The front end display title of this table rate shown to customers. Leave blank to default to the <em>Title</em> from above.'),
    '#size' => 32,
  );

  $form['table_rate']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Describe this table rate if necessary. The text will be displayed in the table rate services overview table.'),
    '#default_value' => $shipping_service['description'],
    '#rows' => 3,
  );

  // Build a currency options list from all enabled currencies.
  $currency_options = commerce_currency_get_code(TRUE);

  // If the current currency value is not available, add it now with a
  // message in the help text explaining it.
  if (isset($shipping_service['data']['currency_code']) && !array_key_exists($shipping_service['data']['currency_code'], $currency_options)) {
    $currency_options[$shipping_service['data']['currency_code']] = check_plain($shipping_service['data']['currency_code']);
    $description = t('The currency set for this rate (@currency_code) is not currently enabled. If you change it now, you will not be able to set it back.', array('@currency_code' => $currency_options[$shipping_service['data']['currency_code']]));
  }
  else {
    $description = t('Selected currency code');
  }

  $form['table_rate']['currency_code'] = array(
    '#type' => 'select',
    '#title' => t('Use this currency code'),
    '#description' => $description,
    '#options' => $currency_options,
    '#default_value' => isset($shipping_service['data']['currency_code']) ? $shipping_service['data']['currency_code'] : commerce_default_currency(),
    '#suffix' => '</div>',
  );

  // Add support for base rates including tax.
  if (module_exists('commerce_tax')) {
    // Build an array of tax types that are display inclusive.
    $inclusive_types = array();

    foreach (commerce_tax_types() as $name => $tax_type) {
      if ($tax_type['display_inclusive']) {
        $inclusive_types[$name] = $tax_type['title'];
      }
    }

    // Build an options array of tax rates of these types.
    $tax_options = array();

    foreach (commerce_tax_rates() as $name => $tax_rate) {
      if (in_array($tax_rate['type'], array_keys($inclusive_types))) {
        $tax_options[$inclusive_types[$tax_rate['type']]][$name] = t('Including @title', array('@title' => $tax_rate['title']));
      }
    }

    $form['table_rate']['include_tax'] = array(
      '#type' => 'select',
      '#title' => t('Include tax in this rate'),
      '#description' => t('Saving a rate tax inclusive will bypass later calculations for the specified tax.'),
      '#options' => count($tax_options) == 1 ? reset($tax_options) : $tax_options,
      '#default_value' => !empty($shipping_service['data']['include_tax']) ? $shipping_service['data']['include_tax'] : '',
      '#required' => FALSE,
      '#empty_value' => '',
    );
  }

  //Check if the service_key_name has been changed by ajax
  /*if (isset($form_state['values']['table_rate']['service_key_name'])) {
    $selected_shipping_service = $form_state['values']['table_rate']['service_key_name'];
  }
  else {
    $selected_shipping_service = isset($shipping_service['service_key_name']) ? $shipping_service['service_key_name'] : '';
  }

  $form['table_rate']['service_key_name'] = array(
    '#type' => 'select',
    '#title' => t('Rates table'),
    '#description' => 'Rate table to use',
    '#options' => commerce_table_rate_services_rates_tables_options(),
    '#default_value' => $selected_shipping_service,
    '#ajax' => array(
      'callback' => 'ajax_recommendation_level_select_callback',
      'wrapper' => 'recommendation-level-div',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );*/

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save table rate'),
  );

  if (!empty($form_state['shipping_service']['name'])) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete table rate'),
      '#suffix' => l(t('Cancel'), 'admin/commerce/config/shipping/services/table-rate'),
      '#submit' => array('commerce_table_rate_service_form_delete_submit'),
      '#weight' => 45,
    );
  }
  else {
    $form['actions']['submit']['#suffix'] = l(t('Cancel'), 'admin/commerce/config/shipping/services/table-rate');
  }

  return $form;
}

/**
 * Validate handler: ensures a valid currency was selected for the service.
 */
function commerce_table_rate_service_form_validate($form, &$form_state) {
  $currency_options = commerce_currency_get_code(TRUE);
  if (!array_key_exists($form_state['values']['table_rate']['currency_code'], $currency_options)) {
    form_set_error('table_rate][currency_code', t('This currency code is not enabled on your site - please select an enabled currency code.'));
  }
}

/**
 * Submit handler: saves the new or updated table rate service.
 */
function commerce_table_rate_service_form_submit($form, &$form_state) {
  $shipping_service = $form_state['shipping_service'];

  // Update the shipping service array with values from the form.
  //foreach (array('name', 'title', 'display_title', 'description', 'service_key_name') as $key) {
  foreach (array('name', 'title', 'display_title', 'description') as $key) {
    $shipping_service[$key] = $form_state['values']['table_rate'][$key];
  }

  unset($shipping_service['data']);
  //$shipping_service['data']['recommendation'] = !empty($form_state['values']['colissimo']['recommendation']) ? $form_state['values']['colissimo']['recommendation'] : '';
  //$shipping_service['data']['smart_calculation'] = $form_state['values']['colissimo']['smart_calculation'];
  $shipping_service['data']['include_tax'] = $form_state['values']['table_rate']['include_tax'];
  $shipping_service['data']['currency_code'] = $form_state['values']['table_rate']['currency_code'];

  // Save the shipping service.
  $op = commerce_table_rate_service_save($shipping_service);

  if (!$op) {
    drupal_set_message(t('The table rate service failed to save properly. Please review the form and try again.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  else {
    drupal_set_message(t('Table rate service saved.'));
    $form_state['redirect'] = 'admin/commerce/config/shipping/services/table-rate';
  }
}

/**
 * Submit handler: redirects to the table rate service delete confirmation form.
 *
 * @see commerce_table_rate_service_form()
 */
function commerce_table_rate_service_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/commerce/config/shipping/services/table-rate-' . strtr($form_state['shipping_service']['name'], '_', '-') . '/delete';
}

/**
 * Displays the edit form for an existing table rate service.
 *
 * @param $name
 *   The machine-name of the table rate service to edit.
 */
function commerce_table_rate_service_edit_page($name) {
  return drupal_get_form('commerce_table_rate_service_form', commerce_shipping_service_load($name));
}

/**
 * Builds the form for deleting table rate services.
 */
function commerce_table_rate_service_delete_form($form, &$form_state, $shipping_service) {
  $form_state['shipping_service'] = $shipping_service;

  $form = confirm_form($form,
    t('Are you sure you want to delete the <em>%title</em> table rate service?', array('%title' => $shipping_service['title'])),
    'admin/commerce/config/shipping/services/table-rate',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_table_rate_service_delete_form().
 */
function commerce_table_rate_service_delete_form_submit($form, &$form_state) {
  $shipping_service = $form_state['shipping_service'];

  commerce_table_rate_service_delete($shipping_service['name']);

  drupal_set_message(t('The table rate service <em>%title</em> has been deleted.', array('%title' => $shipping_service['title'])));
  watchdog('commerce_table_rate', 'Deleted table rate service <em>%title</em>.', array('%title' => $shipping_service['title']), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/commerce/config/shipping/services/table-rate';
}

/**
 * Displays the delete confirmation form for an existing table rate service.
 *
 * @param $name
 *   The machine-name of the table rate service to delete.
 */
function commerce_table_rate_service_delete_page($name) {
  return drupal_get_form('commerce_table_rate_service_delete_form', commerce_shipping_service_load($name));
}

/**
 * Displays the overview admin view to administer the table rate
 */
function commerce_table_rate_overview($service_name) {
  $output = array();

  $rows = array();
  $header = array(
    array('data' => t('Countries'), 'field' => 'ct.countries'),
    array('data' => t('Minimum weight'), 'field' => 'ct.weight_min'),
    array('data' => t('Maximum weight'), 'field' => 'ct.weight_max'),
    array('data' => t('Additional cost'), 'field' => 'ct.cost'),
    array('data' => t('Operations')),
  );
  // Get all rates
  $query = db_select('commerce_table_rate', 'ct')
    ->fields('ct', array('id', 'countries', 'weight_min', 'weight_max', 'cost', 'service_key_name'))
    ->condition('ct.service_key_name', $service_name)
    ->orderBy('countries', 'DESC')
    ->orderBy('cost', 'ASC');
  //  ->execute();
  //dpm($query->fetchAllAssoc('id'));
  $result = $query->execute();

  // Loop through all saved rates and display them
  foreach ($result as $rate) {
    $rows[] = array('data' =>
      array(
        theme('commerce_table_rate_countries', array('countries' => $rate->countries)),
        $rate->weight_min,
        $rate->weight_max,
        $rate->cost,
        theme('commerce_table_rate_links', array(
          'id' => $rate->id,
          'service_name' => $service_name,
        )),
      ),
    );
  }

  $output['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'commerce-table-rate-table'),
    '#empty' => t('No rates defined.'),
  );

  $output += drupal_get_form('commerce_table_rate_settings_form', $service_name);
  return $output;
}

function commerce_table_rate_edit_rate($id, $service_name) {
  $result = commerce_table_rate_load($id);
  if (!$result || !isset($result->id)) {
    return "No rate found!";
  }
  $form = drupal_get_form('commerce_table_rate_settings_form', $service_name);

  $form["table_rate_new"]["#title"] = t("Edit");
  $form["table_rate_new"]["id"]["#value"] = $id;
  $form["table_rate_new"]["destination"]["#value"] = explode(",", $result->countries);
  $form["table_rate_new"]["weight_min"]["#value"] = $result->weight_min;
  $form["table_rate_new"]["weight_max"]["#value"] = $result->weight_max;
  $form["table_rate_new"]["cost"]["#value"] = $result->cost;
  $form["submit"]["#value"] = t("Edit");

  return $form;
}

/**
 * Return form for commerce_table_rate administration.
 *
 * @ingroup forms
 * @see commerce_table_rate_settings_form_submit()
 * @see commerce_table_rate_settings_form_validate()
 */
function commerce_table_rate_settings_form($form, $form_state, $service_name) {
  $countries = country_get_list();

  $form["table_rate_new"] = array(
    '#type' => 'fieldset',
    '#title' => t("New rate"),
  );
  $form["table_rate_new"]["service_key_name"] = array(
    '#type' => 'value',
    '#value' => $service_name,
  );
  $form["table_rate_new"]["id"] = array(
    '#type' => 'hidden',
  );
  $form["table_rate_new"]["destination"] = array(
    '#type' => 'select',
    '#title' => t("Destinations"),
    '#options' => $countries,
    '#multiple' => TRUE,
    '#attributes' => array('class' => array('chosen-widget')),
    '#required' => TRUE,
  );
  $form["table_rate_new"]["weight_min"] = array(
    '#type' => 'textfield',
    '#title' => t("Minimum weight"),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form["table_rate_new"]["weight_max"] = array(
    '#type' => 'textfield',
    '#title' => t("Maximum weight"),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form["table_rate_new"]["cost"] = array(
    '#type' => 'textfield',
    '#title' => t("Additional cost"),
    '#default_value' => '',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );

  return $form;
}

/**
 * Validate result from commerce_table_rate administration form.
 */
function commerce_table_rate_settings_form_validate($form, &$form_state) {
  if (count($form_state['values']['destination']) == 0) {
    form_set_error('destination', t('You must select at least one country.'));
  }
  if (!is_numeric($form_state['values']['weight_min'])) {
    form_set_error('weight_min', t('Minimum weight must be a number.'));
  }
  if (!is_numeric($form_state['values']['weight_max'])) {
    form_set_error('weight_max', t('Maximum weight must be a number.'));
  }
  if (!is_numeric($form_state['values']['cost'])) {
    form_set_error('cost', t('Additional cost must be a number.'));
  }
}

/**
 * Process result from commerce_table_rate administration form.
 */
function commerce_table_rate_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $service_name_arg = 'table-rate-' . strtr($values['service_key_name'], '_', '-');

  if (!empty($values['id'])) {
    db_update('commerce_table_rate')
      ->fields(array(
        'countries' => implode(',', $values['destination']),
        'weight_min' => $values['weight_min'],
        'weight_max' => $values['weight_max'],
        'cost' => $values['cost'],
        'service_key_name' => $values['service_key_name'],
      ))
      ->condition('id', $values['id'])
      ->execute();
      drupal_goto('admin/commerce/config/shipping/services/' . $service_name_arg . '/table/');
  }
  else {
    $id = db_insert('commerce_table_rate')
    ->fields(array(
      'countries' => implode(',', $values['destination']),
      'weight_min' => $values['weight_min'],
      'weight_max' => $values['weight_max'],
      'cost' => $values['cost'],
      'service_key_name' => $values['service_key_name'],
    ))
    -> execute();
  }
}

/**
 * Form builder for the term delete form.
 *
 * @ingroup forms
 * @see commerce_table_rate_confirm_delete_submit()
 */
function commerce_table_rate_confirm_delete($form, &$form_state, $id, $service_name) {
  $service_name_arg = 'table-rate-' . strtr($service_name, '_', '-');
  $rate = commerce_table_rate_load($id);

  $form['id'] = array('#type' => 'value', '#value' => $id);
  $form['#rate'] = $rate;
  $form['type'] = array('#type' => 'value', '#value' => 'rate');
  $form['cost'] = array('#type' => 'value', '#value' => $rate->cost);
  $form['countries'] = array('#type' => 'value', '#value' => theme('commerce_table_rate_countries', array('countries' => $rate->countries)));
  $form['service_name'] = array('#type' => 'value', '#value' => $service_name);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
    t('Are you sure you want to delete the rate (%cost for %countries)?',
    array('%cost' => $rate->cost, '%countries' => theme('commerce_table_rate_countries', array('countries' => $rate->countries)))),
    'admin/commerce/config/shipping/services/' . $service_name_arg . '/table',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * Submit handler to delete a term after confirmation.
 *
 * @see commerce_table_rate_confirm_delete()
 */
function commerce_table_rate_confirm_delete_submit($form, &$form_state) {
  commerce_table_rate_delete($form_state['values']['id']);
  $service_name = $form['service_name']["#value"];
  $service_name_arg = 'table-rate-' . strtr($service_name, '_', '-');
  drupal_set_message(t('Deleted rate (%cost for %countries).', array('%cost' => $form_state['values']['cost'], '%countries' => $form_state['values']['countries'])));
  watchdog('commerce_table_rate', 'Deleted rate (%cost for %countries).', array('%cost' => $form_state['values']['cost'], '%countries' => $form_state['values']['countries']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/commerce/config/shipping/services/' . $service_name_arg . '/table';
  return;
}
