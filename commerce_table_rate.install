<?php

// TODO: use the service_key_name to define which table to use ? or remove it...

/**
 * @file
 * Defines the database schema for commerce table rate shipping.
 */

/**
 * Implements hook_schema().
 */
function commerce_table_rate_schema() {
  $schema = array();

  $schema['commerce_table_rate'] = array(
    'description' => 'Stores information about shipping services created through the Colissimo module.',
    'fields' => array(
      'id' => array(
        'description' => 'The identifier for the rate.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'countries' => array(
        'description' => 'Country code list separated by comma.',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
      ),
      'weight_min' => array(
        'description' => 'The minimum weight range.',
        'type' => 'float',
        'not null' => TRUE,
        'size' => 'medium',
      ),
      'weight_max' => array(
        'description' => 'The maximum weight range.',
        'type' => 'float',
        'not null' => TRUE,
        'size' => 'medium',
      ),
      'cost' => array(
        'description' => 'The additional cost for this specific range.',
        'type' => 'float',
        'not null' => TRUE,
        'size' => 'medium',
      ),
      'service_key_name' => array(
        'description' => 'Name of the service associated with this table rate.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['commerce_table_rate_service'] = array(
    'description' => 'Stores information about shipping services created through the table rate module.',
    'fields' => array(
      'name' => array(
        'description' => 'The machine-name of the table rate service.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'The human-readable title of the table rate service.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'display_title' => array(
        'description' => 'The title of the table rate service displayed to customers.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of the table rate service.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
      ),
      /*'service_key_name' => array(
        'description' => 'Name of the service declared by a hook to use.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
      ),*/
      'rules_component' => array(
        'description' => 'Boolean indicating whether or not this service should have a default Rules component for enabling it for orders.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'A serialized array of additional data.',
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('name'),
  );

  return $schema;
}
